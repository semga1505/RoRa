prontera,154,228,0	script	DefenceGameStart	1915,{
   function	my_global_func; 
   
    // $@count = getmapunits(BL_MOB,"prontera",$@units);
    // mes "�������� �� ����� " + $@count;
    // next;
    
    $@monsterWave = 0;
    $@maxMonsterWave = 1000;
    $@map_name$ = "prontera";
    $@start_x = 158;
    $@start_y = 229;
    $@stop_x = 163;
    $@stop_y = 224;
    $@showMobName$ = "SweetKiller";
    $@mob_id = 1002;
    $@amountMob = 1;  
    $@monsterMaxspeed = 400;
	$@gameMode = 100000;   

	// announce "�������� ��������� " + $@monsterWave +" �������",0;
    // areamonster $@map_name$,$@start_x,$@start_y,$@stop_x,$@stop_y,$@showMobName$,$@mob_id,$@amountMob,"DefenceGameStart::OnThisMobDeath"; //�������� �������� �� ������ �������
    // .GID = $@mobid[0]; // ���������� � ������������� 0-�� ������������ �������, ����� ������� ��� �������!
    // // // ��������� ������ ���� strong Poring � ���������� $@por_arr[]. ($@por_arr[1] - �������, $@por_arr[13] - ����� � �.�.)
    // // � ������� ���� ������ �� ����� ��������� NPC ������������ ��� �������������� �� ���, ��� �� �����. ��� �� ����������� ��������� ����� 'setunitdata'.
    // getunitdata .GID,$@por_arr;
    // // ���������� ������������ �������� ��������� �� 1000 �.�. (������� �������� ����� ����� ��������� �� 1000).
    // setunitdata .GID,UMOB_MAXHP,100;    
    // close;         

    OnThisMobDeath:
    sleep(500);
    $@countDeath = getmapunits(BL_MOB,"prontera",$@units);  
    if ($@countDeath == 0) 
    {
        announce "�� ����� ���� ��������, ����� 5 ������ ��������� " +$@monsterWave + " �����",0;
		$@count = getmapunits(BL_PC,"prontera",$@units); //�������� ����� ���������� ($@count) � �������� � ��������� ������ � ����������� 
		announce "monsterwave " + ($@monsterWave + 1) + " �����",0;
		if((($@monsterWave + 1) % 10) == 0)
		{
			announce "����������� �� ������ " + ($@monsterWave + 1) + " �������� � ��������� �� " +($@gameMode / $@countDeath)+ " zeny",0;
			for (.@i = 0; .@i < $@count; .@i++) 
            {	
				attachrid $@units[.@i];
				Zeny += ($@gameMode / $@countDeath);			  
		    } 
		}
       
        sleep(5000);
        
        if($@monsterWave <= 1000 ){$@monsterWave += my_global_func($@map_name$,$@start_x,$@start_y,$@stop_x,$@stop_y,$@showMobName$,$@mob_id,$@amountMob); }
        
    }
    end;	
	
	function	my_global_func	{
    areamonster getarg(0),getarg(1),getarg(2),getarg(3),getarg(4),getarg(5),1002,getarg(7),"DefenceGameStart::OnThisMobDeath"; //�������� �������� �� ������ �������
    .GID = $@mobid[0]; // ���������� � ������������� 0-�� ������������ �������, ����� ������� ��� �������!
    // // ��������� ������ ���� strong Poring � ���������� $@por_arr[]. ($@por_arr[1] - �������, $@por_arr[13] - ����� � �.�.)
    // � ������� ���� ������ �� ����� ��������� NPC ������������ ��� �������������� �� ���, ��� �� �����. ��� �� ����������� ��������� ����� 'setunitdata'.
    getunitdata .GID,$@por_arr;
    // ���������� ������������ �������� ��������� �� 1000 �.�. (������� �������� ����� ����� ��������� �� 1000).
    setunitdata .GID,UMOB_LEVEL, 1 * ($@monsterWave / 15);
    setunitdata .GID,UMOB_MAXHP,100 * $@monsterWave;
    if($@monsterWave > 400) {$@monsterMaxspeed = 1;}
    if($@monsterMaxspeed > 1){$@monsterMaxspeed -= $@monsterWave;}
    setunitdata .GID, UMOB_SPEED, $@monsterMaxspeed;
    setunitdata .GID,  UMOB_MODE,0xB695;
    //setunitdata .GID,  UMOB_AI
    //setunitdata .GID,  UMOB_SCOPTION
    //setunitdata .GID,  UMOB_SEX
    //setunitdata .GID,  UMOB_CLASS, 4013;
    //setunitdata .GID,  UMOB_HAIRSTYLE
    //setunitdata .GID,  UMOB_HAIRCOLOR
// setunitdata .GID,  UMOB_HEADBOTTOM
// setunitdata .GID,  UMOB_HEADMIDDLE
// setunitdata .GID,  UMOB_HEADTOP
// setunitdata .GID,  UMOB_CLOTHCOLOR
// setunitdata .GID,  UMOB_SHIELD
 //setunitdata .GID,  UMOB_WEAPON, 1210;
// setunitdata .GID,  UMOB_LOOKDIR
// setunitdata .GID,  UMOB_CANMOVETICK

 // setunitdata .GID,  UMOB_STR, 5 * ($@monsterWave);
 // setunitdata .GID,  UMOB_AGI, 5 * ($@monsterWave);
 // setunitdata .GID,  UMOB_VIT, 5 * ($@monsterWave);
 // setunitdata .GID,  UMOB_INT, 5 * ($@monsterWave);
 // setunitdata .GID,  UMOB_DEX, 5 * ($@monsterWave);
 // setunitdata .GID,  UMOB_LUK, 5 * ($@monsterWave);
// setunitdata .GID,  UMOB_SLAVECPYMSTRMD
// setunitdata .GID,  UMOB_DMGIMMUNE
 // setunitdata .GID,  UMOB_ATKRANGE, (1 + ($@monsterWave / 10));
 setunitdata .GID,  UMOB_ATKMIN, 65500;
 setunitdata .GID,  UMOB_ATKMAX, 65500;
 // setunitdata .GID,  UMOB_MATKMIN, 5 * ($@monsterWave );
 // setunitdata .GID,  UMOB_MATKMAX, 10 * ($@monsterWave );
 // setunitdata .GID,  UMOB_DEF, 2 * ($@monsterWave / 2);
 // setunitdata .GID,  UMOB_MDEF, 5 * ($@monsterWave );
 // setunitdata .GID,  UMOB_HIT, 10 * ($@monsterWave);
 // setunitdata .GID,  UMOB_FLEE, 10* ($@monsterWave / 12);
// // setunitdata .GID,  UMOB_PDODGE
 // //setunitdata .GID,  UMOB_CRIT, 1000;
// // setunitdata .GID,  UMOB_RACE
// // setunitdata .GID,  UMOB_ELETYPE
// // setunitdata .GID,  UMOB_ELELEVEL



 // setunitdata .GID,  UMOB_AMOTION, 1872 - ($@monsterWave * 2);
 
     // if($@monsterWave > 670) {$@UMOB_ADELAYP = 1;}
    // if($@UMOB_ADELAYP > 1){$@UMOB_ADELAYP -= $@monsterWave;}
 
 // setunitdata .GID,  UMOB_ADELAY, $@UMOB_ADELAYP;
 
      // if($@monsterWave > 460) {$@UMOB_DMOTION = 1;}
    // if($@UMOB_DMOTION > 1){$@UMOB_DMOTION -= $@monsterWave;}
 // setunitdata .GID,  UMOB_DMOTION, $@UMOB_DMOTION;
// setunitdata .GID,  UMOB_TARGETID
// setunitdata .GID,  UMOB_ROBE
// setunitdata .GID,  UMOB_BODY2
// setunitdata .GID,  UMOB_GROUP_ID
// setunitdata .GID,  UMOB_IGNORE_CELL_STACK_LIMIT
// setunitdata .GID,  UMOB_RES
// setunitdata .GID,  UMOB_MRES
// setunitdata .GID,  UMOB_DAMAGETAKEN
    return 1;

}


}

